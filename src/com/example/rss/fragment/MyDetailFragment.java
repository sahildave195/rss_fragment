package com.example.rss.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MyDetailFragment extends Fragment {
	private static final String TAG = "RSS_FRAGMENT";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.webview_fragment, container, false);
		return view;
	}

	public void setText(String title, String article) {

		TextView articleHeadline = (TextView) getView().findViewById(R.id.articleHeadline);
		articleHeadline.setText(title);

		WebView webView = (WebView) getView().findViewById(R.id.webViewArticle);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});

		webView.loadUrl(article);

	}
}