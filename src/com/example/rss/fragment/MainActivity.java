package com.example.rss.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements MyListFragment.OnItemSelectedListener {
	private static final String TAG = "RSS_FRAGMENT";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	@Override
	public void onRssItemSelected(String title, String article) {

		MyDetailFragment fragment = (MyDetailFragment) getFragmentManager().findFragmentById(R.id.mydetailFragment);

		if (fragment != null && fragment.isInLayout()) {
			fragment.setText(title, article);
		}

	}

}