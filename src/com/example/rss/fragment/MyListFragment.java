package com.example.rss.fragment;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MyListFragment extends ListFragment {

	private static final String TAG = "RSS_FRAGMENT";
	ListView listView;

	private OnItemSelectedListener listener;
	ArrayList<String> linkArrayList;
	ArrayList<String> titleArrayList;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		// MyAdapter adapter = new MyAdapter(new ArrayList<String>());
		super.onActivityCreated(savedInstanceState);

		final String cricketURL = "http://timesofindia.feedsportal.com/c/33039/f/533920/index.rss";

		ConnectivityManager connMgr = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			new MyAsyncTask().execute(cricketURL);
		} else {
			Log.d(TAG, "No network connection available.");
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnItemSelectedListener) {
			listener = (OnItemSelectedListener) activity;
		} else {
			throw new ClassCastException(activity.toString() + " must implemenet MyListFragment.OnItemSelectedListener");
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String title = (String) titleArrayList.get(position);
		String article = (String) linkArrayList.get(position);

		updateDetail(title, article);

	}

	public void updateDetail(String title, String article) {
		// Send data to Activity
		listener.onRssItemSelected(title, article);
	}

	public interface OnItemSelectedListener {
		public void onRssItemSelected(String title, String article);
	}

	private class MyAsyncTask extends AsyncTask<String, Void, ArrayList<String>> {

		@Override
		protected ArrayList<String> doInBackground(String... args) {
			Log.d(TAG, "In doBackground ");

			titleArrayList = new ArrayList<String>();
			linkArrayList = new ArrayList<String>();

			try {
				XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();

				InputStream streamUrl = loadXmlFromNetwork(args[0]);
				Log.d(TAG, "stream " + streamUrl.toString());
				xpp.setInput(streamUrl, null);

				beginDocument(xpp, "item");

				do {
					if (xpp.getEventType() == XmlPullParser.START_TAG && xpp.getName().equals("title")) {
						xpp.next();

						titleArrayList.add(xpp.getText());
					} else if (xpp.getEventType() == XmlPullParser.START_TAG && xpp.getName().equals("link")) {
						xpp.next();

						linkArrayList.add(xpp.getText());
					}
					xpp.next();
				} while (xpp.getEventType() != XmlPullParser.END_DOCUMENT);

			} catch (Throwable t) {
				Log.d(TAG, t.toString());
			}
			return titleArrayList;
		}

		private void beginDocument(XmlPullParser xpp, String string) throws XmlPullParserException, IOException {

			int type;
			while ((xpp.next() != xpp.END_DOCUMENT)) {
				if ((xpp.getEventType() == xpp.START_TAG) && (xpp.getName().equals("item"))) {
					return;
				}
			}
		}

		private InputStream loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
			InputStream stream = null;
			try {

				URL url = new URL(urlString);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				// Starts the query
				conn.connect();
				stream = conn.getInputStream();

			} finally {
			}

			return stream;

		}

		@Override
		protected void onPostExecute(final ArrayList<String> result) {

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_row, result) {
				public View getView(int position, View convertView, ViewGroup parent) {

					View v = convertView;
					if (v == null) {
						v = getLayoutInflater(null).inflate(R.layout.list_row, parent, false);
					}
					TextView headline = (TextView) v.findViewById(R.id.headline);
					headline.setText(result.get(position));

					return v;
				}
			};
			setListAdapter(adapter);
		}

	}

}
